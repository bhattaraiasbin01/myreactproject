import { Form, Formik } from 'formik';
import React from 'react'
import * as yup from "yup";
import { clothOption, genderOption } from '../config/Config';
import FormikCheckBox from './FormikCheckBox';
import FormikInput from './FormikInput';
import FormikRadio from './FormikRadioBox';
import FormikSelect from './FormikSelect';
import FormikTextArea from './FormikTextArea';

const FormikComponentAll = () => {
    let initialValues = {
        email: "",
        phoneNumber: "",
        password: "",
        isMarried: false,
        clothSize: "S",
        gender: "male",
        comment: "",
        role: "admin",
        age: 0,
    }
    let onSubmit = (value, other) => {
        console.log(value)
        
    }
    let validationSchema = yup.object({
        email: yup.string().required("Email is Required.")
            .matches(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/, "Invalid Email"),
        phoneNumber: yup.string().required("Phone Number is required")
            .min(10, "Phone Number must at least 10 digits")
            .max(10, "Phone Number must at most 10 digits")
        .matches("^[0-9]+$","Only numbers are allowed"),
        password: yup.string().required("Password is required")
            .min(8, "Password must have at least character")
            .max(15, "Password must have at most 15 character")
        .matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$","Password must have minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"),
        isMarried: yup.boolean(),
        clothSize: yup.string().required("Cloth Size is required"),
        gender: yup.string().required("Gender is required"),
        comment: yup.string(),
        role: yup.string().required("Role is required"),
        age: yup.number().required("Age is required."),
    });
    
 
    let roleOption = [
        {
            label: "Select Role",
            value: "",
            disabled: true
        },
        {
            label: "Admin",
            value: "admin"
        },
        {
            label: "Super Admin",
            value: "superAdmin"
        },
        {
            label: "Customer",
            value: "customer"
        },
        {
            label: "Delivery Person",
            value: "deliveryPerson" 
        }
    ]
  return (
      <div>
          <Formik
              initialValues={initialValues}
              onSubmit={onSubmit}
              validationSchema={validationSchema}>
              {(formik) => {
                  return (
                      <Form>
                          {/* name:-must
                          label:-must
                          type:-only in input
                          required
                          onChange:-only if you have multiple task to do i.e. other than settings its value
                          options -only for selecting option */}
                          <FormikInput
                              name="email"
                              label="Email"
                              type="email"
                            //   onChange={(e) => {
                            //       formik.setFieldValue("email", e.target.value) 
                            //   }}
                              required={true}></FormikInput>
                          <FormikInput
                              name="phoneNumber"
                              label="Phone Number"
                              type="text"
                              required={true}></FormikInput>
                           <FormikInput
                              name="password"
                              label="Password"
                              type="password"
                              required={true}></FormikInput>
                          <FormikCheckBox
                              name="isMarried"
                              label="Is Married"
                          ></FormikCheckBox>
                          <FormikSelect
                              name="clothSize"
                              label="Cloth Size"
                              options={clothOption}
                          required={true}>
                              
                          </FormikSelect>
                          <FormikRadio
                              name="gender"
                              label="Gender"
                              options={genderOption}
                              required={true}>
                              
                          </FormikRadio>
                          <FormikTextArea
                              name="comment"
                              label="Comment"
                              type="text">
                          </FormikTextArea>

                          <FormikSelect
                              name="role"
                              label="Role"
                              options={roleOption}
                          required={true}>
                          </FormikSelect>
                          
                          <FormikInput
                              name="age"
                              label="Age"
                              type="number"
                              required={true}>
                            </FormikInput>
                         
                          <button type="submit">Submit</button>
                      </Form>
                  )
       }}
          </Formik>
    </div>
  )
}

export default FormikComponentAll
