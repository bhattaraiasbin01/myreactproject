import { Form, Formik } from 'formik'
import React from 'react'
import FormikCheckBox from './FormikCheckBox'
import FormikInput from './FormikInput'
import FormikSelect from './FormikSelect'
import * as yup from "yup";


const ProductInfo = () => {
    let initialValues = {
        productName: "",
        productPrice: 0,
        productQuantity: 0,
        productCategory: "A",
        isAvailable:true
    }
    let onSubmit = (value, others) => {
        console.log(value)
    }
    let validationSchema = yup.object({
        productName: yup.string().required("Name is Required."),
        productPrice: yup.number().required("Age is required.").min(1,"Price cannot be zero"),
        productQuantity: yup.number().required("Email is required").min(1,"Quantity cannot be zero"),
        productCategory: yup.string().required("Password is required"),
        isAvailable:yup.boolean()
      });
    let categoryOption = [
        {
          label:"A",
          value: "a" 
        },
        {
            label:"B",
            value: "b" 
        },
        {
            label:"C",
            value: "c" 
        },
        {
            label:"D",
            value: "d" 
        },
        {
            label:"E",
            value: "e" 
          },
    ]
  return (
      <div>
          <Formik
              initialValues={initialValues}
              onSubmit={onSubmit}
              validationSchema={validationSchema}>
              {(formik) => {
                  return (
                      <div>
                          <Form>
                              <FormikInput
                                  name="productName"
                                  label="Product Name"
                                  type="text"
                                  required={true}></FormikInput>
                               <FormikInput
                                  name="productPrice"
                                  label="Product Price"
                                  type="number"
                                  required={true}></FormikInput>
                              <FormikInput
                                  name="productQuantity"
                                  label="Product Quantity"
                                  type="number"
                                  required={true}></FormikInput>
                              <FormikSelect
                                  name="productCategory"
                                  label="Product Category"
                                  required={true}
                              options={categoryOption}></FormikSelect>
                              <FormikCheckBox
                                  name="isAvailable"
                                  label="Is Available"
                              ></FormikCheckBox>
                              <button type="submit">Submit</button>
                          </Form>
                      </div>
                  )
              }}
          </Formik>
    </div>
  )
}

export default ProductInfo