// import React from 'react'
import axios from 'axios'
import React, { useEffect } from 'react'
import * as yup from "yup";

// import { Await, Form } from 'react-router-dom'
import { Form, Formik } from 'formik';
import FormikInput from '../FormikHomeWork/FormikInput';
import FormikCheckBox from '../FormikHomeWork/FormikCheckBox';
import FormikSelect from '../FormikHomeWork/FormikSelect';
import ProductForm from './ProductForm';
import { useNavigate } from 'react-router-dom';
import { hitApi } from '../services/HitApi';
import { useCreateProductMutation } from '../services/api/productServices';

const CreateProductUsingRtk = () => {
    let navigate = useNavigate()
    let [createProduct, { isError: isErrorCreateData, isLoading: isLoadingCreateData, isSucess: isSucessCreateData, error: errorCreateData, data: dataCreateData }] = useCreateProductMutation()
    useEffect(() => {
        if (isSucessCreateData) {
            console.log("Successfully created")
        }
    },[isSucessCreateData])
    useEffect(() => {
        if (isErrorCreateData) {
            console.log(errorCreateData.error)
        }
    },[isSucessCreateData,errorCreateData])
  let onSubmit =async (values, other) => { 
    let body=values
      createProduct(body)
      navigate("/products")
  }
 
 
  return (  
    <div>
      <ProductForm
        buttonName='Create Product'
              onSubmit={onSubmit}
          isLoading={isLoadingCreateData}></ProductForm>
    </div>
  )
}

export default CreateProductUsingRtk