import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { hitApi } from '../services/HitApi'

const ShowAllProducts = () => {
    let [products, setProducts] = useState([])
    let navigate=useNavigate()
    let getProduct = async () => {
        try {
            let output=await hitApi({
                method: "get",
                url: "/products",
                
            })
            setProducts(output.data.data.results)
            
        } catch (error) {
            console.log(error.message)
        }

        
        // console.log(output.data.data.results)
}


    useEffect(() => { getProduct() }, [])
    let handleView = (item) => { 
        return (navigate(`/products/${item._id}`)
        )
      

    } 
  return (
      <div>
          {products.map((item, i) => {
              return (
                  <div style={{border:"solid 3px red",marginBottom:"10px"}}>
                      <img alt="loading" src={item.productImage} style={{ height: "1oopx", width: "100px" }}></img>
                      <br></br>
                      name:{item.name}
                      <br></br>
                      price:{item.price}
                      <br></br>
                      quantity:{item.quantity}
                      <br></br>
                      company:{item.company}
                      <br></br>
                      <button onClick={async () => {
                          try {
                            await hitApi({
                                method: "delete",
                                url:`products/${item._id}`
                            })
                              getProduct()
                            
                          } catch (error) {
                              console.log("error")
                            
                          }
                      }}>Delete Product</button>
                      {/* invaildation means hitting two api one after another here first we hit delete api and then hit getProduct api  */}
                          <br></br>
                      <button onClick={handleView(item)}>View Product</button>
                      <button onClick={() => {
                          navigate(`/products/update/${item._id}`)
                
                          }}>Edit Product</button>
                  </div>
              )
          })}
    </div>
  )
}

export default ShowAllProducts