// import React from 'react'
import axios from 'axios'
import React, {  useEffect, useState } from 'react'
import * as yup from "yup";

// import { Await, Form } from 'react-router-dom'
import { Form, Formik } from 'formik';
import FormikInput from '../FormikHomeWork/FormikInput';
import FormikCheckBox from '../FormikHomeWork/FormikCheckBox';
import FormikSelect from '../FormikHomeWork/FormikSelect';
import { useNavigate, useParams } from 'react-router-dom';
import ProductForm from './ProductForm';
import { hitApi } from '../services/HitApi';
import { useUpdateProductMutation } from '../services/api/productServices';

const UpdateProductUsingRtk = () => {
    let [product, setProduct] = useState([])
    let navigate=useNavigate()
    let params = useParams()
    let id = params.id
    
    let [updateProduct, { isError: isErrorUpdateData, isLoading: isLoadingUpdateData, isSucess: isSucessUpdateData, error: errorUpdateData, data: dataUpdateData }] =     useUpdateProductMutation()

    useEffect(() => {
        if (isSucessUpdateData) {
            console.log("Updated Successfully")
        }
    },[isSucessUpdateData])
    useEffect(() => {
        if (isErrorUpdateData) {
            console.log(errorUpdateData.error)
        }
    },[isSucessUpdateData,errorUpdateData])
 
    let getProduct = async () => {
        try {
          let output = await hitApi({
            method: "get",
            url:`/products/${id}`
          })
          setProduct(output.data.data)
          //console.log(output.data.data)
          
        } catch (error) {
          console.log(error.message)
        }
       
    }
    useEffect(() => {
        getProduct()
    })
    let onSubmit = async (values, other) => { 
        
    // console.log(value)
        updateProduct({ id: id, body: values })
        navigate(`product/${id}`)
   
  }
 
   
  return (
    <div>
      <ProductForm
        buttonName='Update Product'
        onSubmit={onSubmit}
              product={product}
          isLoading={isLoadingUpdateData}></ProductForm>
    </div>
  )
}

export default UpdateProductUsingRtk