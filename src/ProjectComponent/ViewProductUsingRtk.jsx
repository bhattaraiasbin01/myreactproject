import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useReadProductByIdQuery } from '../services/api/productServices'


const ViewProductUsingRtk = () => {
  let params = useParams();
    let id = params.id
    let { isLoading: isLoadingReadDetails, isSuccess: isSuccessReadDetails, isError: isErrorReadDetails, error: errorReadDetails, data: dataReadDetails } = useReadProductByIdQuery(id)
    // console.log(dataReadDetails?.data)  
    let product=dataReadDetails?.data || {}

  useEffect(() => { 
      if (isErrorReadDetails) {
        console.log(errorReadDetails.error)
    }
  },[isErrorReadDetails,errorReadDetails])
    return (
        <div>
            {isLoadingReadDetails?<div><h1>...isLoading</h1></div>: <div>
      <img alt="loading error" src={product.productImage} style={{width:"100px",height:"100px"}}></img>
     <p> Product Name:{product.name}</p> 
    <p> Product Company:{product.company}</p> 
      <p> isInFeatured:{product.featured ? "yes" : "no"}</p> 
      <p> manufactureDate:{new Date(product.manufactureDate).toLocaleDateString()}</p> 
      <p>Price: NRs. { product.price}</p>
    <p> Product Quantity:{product.quantity}</p> 
      
      
            </div>}
   
            </div>
  )
}

export default ViewProductUsingRtk