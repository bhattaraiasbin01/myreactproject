// import React from 'react'
import axios from 'axios'
import React from 'react'
import * as yup from "yup";

// import { Await, Form } from 'react-router-dom'
import { Form, Formik } from 'formik';
import FormikInput from '../FormikHomeWork/FormikInput';
import FormikCheckBox from '../FormikHomeWork/FormikCheckBox';
import FormikSelect from '../FormikHomeWork/FormikSelect';
import ProductForm from './ProductForm';
import { useNavigate } from 'react-router-dom';
import { hitApi } from '../services/HitApi';

const CreateProduct = () => {
  let navigate=useNavigate()
  let onSubmit =async (values, other) => { 
    // console.log(value)
    try {
      let output= await hitApi({
        method: "post",
        url: "/products",
        data:values 
      })
      navigate("/products")
      
    }
    catch (error) {
      console.log(error.message)
      
    }
   
  }
 
 
  return ( 
    <div>
      <ProductForm
        buttonName='Create Product'
      onSubmit={onSubmit}></ProductForm>
    </div>
  )
}

export default CreateProduct