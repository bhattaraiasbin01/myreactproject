import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { hitApi } from '../services/HitApi'
//get,
// "https://project-dw.onrender.com/api/v1/products/${id}",
const ViewProduct = () => {
  let[product,setProduct]=useState({})
  let params = useParams()
  let id = params.id
  let getProduct = async () => {
    try {
      let output = await hitApi({
        method: "get",
        url:`/products/${id}`
      })
      setProduct(output.data.data)
      //console.log(output.data.data)
      
    } catch (error) {
      console.log(error.message)
    }
   
  }
  useEffect(() => {
    getProduct()
  },[])
  return (
    <div>
      <img alt="loading error" src={product.productImage} style={{width:"100px",height:"100px"}}></img>
     <p> Product Name:{product.name}</p> 
    <p> Product Company:{product.company}</p> 
      <p> isInFeatured:{product.featured ? "yes" : "no"}</p> 
      <p> manufactureDate:{new Date(product.manufactureDate).toLocaleDateString()}</p> 
      <p>Price: NRs. { product.price}</p>
    <p> Product Quantity:{product.quantity}</p> 
      
      
    </div>
  )
}

export default ViewProduct