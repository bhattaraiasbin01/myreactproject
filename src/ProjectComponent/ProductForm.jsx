// import React from 'react'
import axios from 'axios'
import React from 'react'
import * as yup from "yup";

// import { Await, Form } from 'react-router-dom'
import { Form, Formik } from 'formik';
import FormikInput from '../FormikHomeWork/FormikInput';
import FormikCheckBox from '../FormikHomeWork/FormikCheckBox';
import FormikSelect from '../FormikHomeWork/FormikSelect';
 import htmlDateFormat from '../ProjectUtils/htmlDateFormat'
import { companyOptions } from '../config/Config';

const ProductForm = ({buttonName="Create Product",onSubmit=()=>{},product={},isLoading=false}) => {
  let initialValues = {
    name: product.name || "",
    quantity: product.quantity || 1,
    price: product.price || 100,
    featured: product.featured || false,
    productImage: product.productImage,
    manufactureDate: htmlDateFormat(product.manufactureDate || new Date()),
    company:product.company || "apple"
    
    }
  
  let validationSchema = yup.object({
    name: yup.string().required("Name is required"),
    quantity: yup.number().required("Quantity is required").min(1,"Quantity cannot be zero"),
    price: yup.number().required("Age is required.").min(1,"Price cannot be zero"),
      // yup.number().required("price is required."),
    featured: yup.boolean(),
    productImage: yup.string().required("Product Image is required"),
    manufactureDate: yup.string().required("Date is required"),
    company: yup.string().required("Company is required"),
  });

  return ( 
    <div>
      <Formik
        initialValues={initialValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      enableReinitialize={true}>
        {(formik) => {
          return (
            <div>
              <Form>
                <FormikInput
                  name="name"
                  label="Name"
                  type="text"
                required={true}></FormikInput>
                  <FormikInput
                    name="quantity"
                    label="Quantity"
                    type="number"
                    required={true}></FormikInput>
                <FormikInput
                  name="price"
                  label="Price"
                  type="number"
                  required={true}></FormikInput>
                <FormikCheckBox
                  name="featured"
                  label="featured"></FormikCheckBox>
                <FormikInput
                  name="productImage"
                  label="Product Image"
                  type="text"
                  required={true}></FormikInput>
                <FormikInput
                  name="manufactureDate"
                  label="Manufacture Date"
                  type="date"
                  required={true}></FormikInput>
                <FormikSelect
                name="company"
                label="Company"
                required={true}
                options={companyOptions}></FormikSelect>
                <button type="submit">
                  {
                    isLoading ? <div>{ buttonName}....</div>:<div>{buttonName}</div>}</button>
                
                
              </Form>
            </div>
          )
        }}
        </Formik>
    </div>
  )
}

export default ProductForm