import React, { useEffect} from 'react'
import { useNavigate } from 'react-router-dom'
import { useDeleteProductMutation, useReadProductQuery } from '../services/api/productServices'

const ShowAllProductUsingRtk
    = () => {
        let { isError:isErrorReadProduct, isSuccess:isSuccessReadProduct, isLoading:isLoadingReadProduct, data:dataReadProduct, error:errorReadProduct } = useReadProductQuery() //query gives object
        let [deletePorduct,{ isError:isErrorDeleteProduct, isSuccess:isSuccessDeleteProduct, isLoading:isLoadingDeleteProduct, data:dataDeleteProduct, error:errorDeleteProduct } ]= useDeleteProductMutation() //mutation gives array and  object always
        useEffect(() => {
            if (isErrorReadProduct) {
                console.log(errorReadProduct?.error)
            }
            },[isErrorReadProduct,errorReadProduct] )
        useEffect(() => {
            if (isErrorDeleteProduct) {
                console.log(errorDeleteProduct?.error)
            }
        }, [isErrorDeleteProduct, errorDeleteProduct])
        useEffect(() => {
            if (isSuccessDeleteProduct) {
                console.log("Product Deleted Successfully")
            }
        },[isSuccessDeleteProduct])
     let products= dataReadProduct?.data?.results
    let navigate=useNavigate()
 
    let handleView = (item) => { 
        return (navigate(`/products/${item._id}`)
        )
        } 
        let handleEdit=(item) => {
            navigate(`/products/update/${item._id}`)
  
            }
        
        return (
            <div>
                {
                    isLoadingReadProduct?<div><h1>...Loading</h1></div>: <div>
                    {products?.map((item, i) => {
                        return (
                            <div
                                key={i}
                                style={{ border: "solid 3px red", marginBottom: "10px" }}>
                                <img alt="loading" src={item.productImage} style={{ height: "1oopx", width: "100px" }}></img>
                                <br></br>
                                name:{item.name}
                                <br></br>
                                price:{item.price}
                                <br></br>
                                quantity:{item.quantity}
                                <br></br>
                                company:{item.company}
                                <br></br>
                                <button onClick={async (e) => {
                                    deletePorduct(item._id);

                                    
                                }}> { isLoadingDeleteProduct?"Deleting...":"Delete Product"}</button>
                                {/* invaildation means hitting two api one after another here first we hit delete api and then hit getProduct api  */}
                                    <br></br>
                                <button onClick={handleView(item)}>View Product</button>
                                <button onClick={handleEdit(item)}>Edit Product</button>
                            </div>
                        )
                    })}
              </div>
                }
     
    </div>
  )
}

export default ShowAllProductUsingRtk
