import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
// import App from "./App";
import reportWebVitals from "./reportWebVitals";
import "./new.css";
// import NewApp from "./NewApp";
import AppApp from "./AppApp";
import { BrowserRouter } from "react-router-dom";
import Project from "./Project";
import { Provider } from "react-redux";
import { store } from "./Store/store";
// import "./ABCD/ram.css";

//writing path :- (.) means same folder (/) means about to write smthing

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  // <React.StrictMode>
  // <App />
  // <NewApp></NewApp>
  // </React.StrictMode>
  <Provider store={store}>
    <BrowserRouter>
      {/* <AppApp></AppApp> */}
      <Project></Project>
    </BrowserRouter>
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
