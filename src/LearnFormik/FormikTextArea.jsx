import { Field} from "formik";

import React from "react";
// import * as yup from "yup";

const FormikTextArea = ({name,label,type,onChange,required,...props}) => {
  return (
    <div>
  
              <Field name={name}>
                {({ field, form, meta }) => {
                  return (
                    <div>
                      <label htmlFor={name}>{label}
                        {required ? <span style={{color:"red"}}>*</span> : null}</label>
                      <textarea
                        id={name}
                        type={type}
                        {...field}
                        {...props}
                        value={meta.value}
                        onChange={onChange? onChange:field.onChange}
                     
                      ></textarea>
                     

                      {meta.touched && meta.error ? (
                        <div style={{ color: "red" }}>{meta.error}</div>
                      ) : null}
                    </div>
                  );
                }}
      </Field>
      

    </div>
    
          );
        }
     

export default FormikTextArea;