import { Field} from "formik";

import React from "react";
// import * as yup from "yup";

const FormikCheckBox = ({name,label,onChange,required,...props}) => {
  return (
      <div>
            <Field name={name}>
                {({ field, form, meta }) => {
                  return (
                      <div>
                          <label htmlFor={name}>{label}
                        {required ? <span style={{color:"red"}}>*</span> : null}</label>
                          <input
                              {...props}
                              {...field}

                  id={name}
                  type='checkbox'
                
                              onChange={onChange ? onChange : field.onChange}
                              checked={meta.value}
                  ></input>
                      </div>
                  );
                }}
      </Field>

    </div>
    
          );
        }
     

export default FormikCheckBox;