// in redux slice means creating variable in store and initilize that.

//defining store variable and initilize it

//the main task of slice is
//to initilize variable
// and to reinitilize variable
import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  format: "test",
  over: 90,
};

export const counterSlice = createSlice({
  // give name unique
  name: "cricketSlice",
  //this name can be any ,note the type of action will be name/reducer eg counteryou/increment ....no need to care about
  //just remeber use name unique
  initialState: initialStateValue,
  reducers: {
    changeFormat: (state, action) => {
      state.format = action.payload;
    },
    changeOver: (state, action) => {
      state.over = action.payload;
    },
  },
});

export const { changeFormat, changeOver } = counterSlice.actions;

export default counterSlice.reducer;
