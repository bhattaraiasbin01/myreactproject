// in redux slice means creating variable in store and initilize that.

//defining store variable and initilize it

//the main task of slice is
//to initilize variable
// and to reinitilize variable
import { createSlice } from "@reduxjs/toolkit";

const initialStateValue = {
  fName: "MoMo",
  fType: "snacks",
};

export const counterSlice = createSlice({
  // give name unique
  name: "foodSlice",
  //this name can be any ,note the type of action will be name/reducer eg counteryou/increment ....no need to care about
  //just remeber use name unique
  initialState: initialStateValue,
  reducers: {
    changeFname: (state, action) => {
      state.fName = action.payload;
    },
    changeFtype: (state, action) => {
      state.fType = action.payload;
    },
  },
});

export const { changeFname, changeFtype } = counterSlice.actions;

export default counterSlice.reducer;
