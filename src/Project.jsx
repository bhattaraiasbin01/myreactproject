import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Outlet, Route, Routes } from 'react-router-dom'
import { changeName } from './feature/counterSlice'
import { changeFormat, changeOver } from './feature/cricketSlice'
import { changeFname, changeFtype } from './feature/foodSlice'
import CreateProduct from './ProjectComponent/CreateProduct'
import CreateProductUsingRtk from './ProjectComponent/CreateProductUsingRtk'
import NavBar from './ProjectComponent/NavBar'
// import ShowAllProducts from './ProjectComponent/ShowAllProducts'
import ShowAllProductUsingRtk from './ProjectComponent/ShowAllProductUsingRtk'
import UpdateProduct from './ProjectComponent/UpdateProduct'
import UpdateProductUsingRtk from './ProjectComponent/UpdateProductUsingRtk'
// import ViewProduct from './ProjectComponent/ViewProduct'
import ViewProductUsingRtk from './ProjectComponent/ViewProductUsingRtk'

const Project = () => {
    // let dispatch=useDispatch()
    // let info = useSelector((store) => {
    //     return store.info
    // })
    // let cricket = useSelector((store) => {
    //     return store.cricket
    // })
    // let food = useSelector((store) => {
    //     return store.food
    // })
    // console.log(info)
    return (
      <div>
            <NavBar></NavBar>
            <Routes>
                <Route path="/" element={<div><Outlet></Outlet></div>}>
                    <Route index element={<div>Home Page</div>}></Route>
                    <Route path="products" element={<div><Outlet></Outlet></div>}>
                        {/* <Route index element={<div><ShowAllProducts></ShowAllProducts></div>}></Route> */}
                        <Route index element={<div><ShowAllProductUsingRtk></ShowAllProductUsingRtk></div>}></Route>
                        {/* <Route path=":id" element={<ViewProduct></ViewProduct>}></Route> */}
                        <Route path=":id" element={<ViewProductUsingRtk></ViewProductUsingRtk>}></Route>
                        {/* <Route path="create" element={<div> <CreateProduct></CreateProduct></div>}></Route> */}
                        <Route path="create" element={<div> <CreateProductUsingRtk></CreateProductUsingRtk></div>}></Route>
                        <Route path="update" element={<div><Outlet></Outlet></div>}>
                            <Route index element={<div>Update Product</div>}></Route>
                            {/* <Route path=":id" element={<UpdateProduct></UpdateProduct>}> */}
                            <Route path=":id" element={<UpdateProductUsingRtk></UpdateProductUsingRtk>}>

                            </Route>
                        </Route>
                    </Route> 
                </Route>
            </Routes>
            {/* <div>
                {info.name}<br></br>
                {info.age}<br></br>
                {cricket.format}<br></br>
                {cricket.over}<br></br>
                {food.fName}<br></br>
                {food.fType}<br></br>
                <button
                    onClick={() => {
                        dispatch(changeName("hari"))
                }}>Change Name</button>
                <button
                    onClick={() => {
                        dispatch(changeFormat("odi"))
                }}>Change Format</button>
                <button
                    onClick={() => {
                        dispatch(changeOver(50))
                }}>Change Over</button>
                <button
                    onClick={() => {
                        dispatch(changeFname("Fried Rice"))
                }}>Change Food Name</button>
                <button
                    onClick={() => {
                        dispatch(changeFtype("Meal"))
                }}>Change Food Type</button>
            </div> */}
            
        </div>
  )
}

export default Project 