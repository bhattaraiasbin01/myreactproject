import React, { useState } from 'react'

const ObjState = () => {
    let [info, setInfo] = useState({
        name: `***`,
        age: 0,
        address:` ***`
    })
  return (
      <div>
          name is {info.name}<br></br>
          age is {info.age} <br></br>
          address is {info.address}<br></br>
          <br></br>
          <button onClick={() => {
              setInfo({
                //   name: `Ram`,
                //   age: info.age,
                //   address:info.address
                ...info,
                name:`Ram`
              })
          }}>Set Name</button>
          <button onClick={() => {
              setInfo({
                //   name: info.name,
                //   age: 20,
                //   address:info.address
                ...info,
                 age: 20,
              })
          }}>Set Age</button>
          <button onClick={() => {
              setInfo({
                //   name: info.name,
                //   age: info.age,
                //   address:`Morang`
                ...info,
                address:`Morang`
              })
          }}>Set Address</button>
    </div>
  )
}

export default ObjState