// import { computeHeadingLevel } from '@testing-library/react'
import React, { useEffect, useState } from 'react'

const ChangeTitle = () => {
    let [documentTitle, setDocumentTitle] = useState("Title1")
    useEffect(() => {
        document.title = documentTitle
        // {documentTitle}
    },[documentTitle])
  return (
      <div>ChangeTitle<br></br>
          <button
              onClick={() => {
                  setDocumentTitle("Title2")
                  
          }}>Change Title</button>
    </div>
  )
}

export default ChangeTitle