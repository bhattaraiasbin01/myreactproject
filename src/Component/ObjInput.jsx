// @41 make a component that is responsible for
// create a state varible  that takes input as object which hold information of productName, productPrice, productQuanty ie {productName:"", productPrice:"", productQuantity:""} and make 3 button to change productName, productPrice and productQuantity


import React, { useState } from 'react'

const ObjInput = () => {
    // let[count,setCount] = useState(0)
    let [productInfo, setProductInfo] = useState({
        productName: "",
        productPrice: 0,
        productQuantity:0
       
    })
  return (
      <div>
          ProductName is {productInfo.productName}<br></br>
          ProductPrice is {productInfo.productPrice}<br></br>
          ProductQuantity is {productInfo.productQuantity}<br></br>
          <button onClick={() => {
              setProductInfo({
                  ...productInfo,
                  productName:`Noodles`
              })
          }}>Product Name</button>
          <button onClick={() => {
              setProductInfo({
                  ...productInfo,
                  productPrice:50
              })
          }}>Product Price</button>
          <button onClick={() => {
              setProductInfo({
                  ...productInfo,
                  productQuantity:productInfo.productQuantity+1
              })
          }}>Product Quantity</button>
        
    </div>
  )
}

export default ObjInput