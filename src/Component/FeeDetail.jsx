import React from 'react'

export const FeeDetail = ({ obj }) => {
    let feeStructure = [
        {
            courseName:` BIT`,
            duration: `4 years`,
            creditHour: `128`,
            Fee:`9,20,000`
        },
        {
            courseName:` BCS`,
            duration: `4 years`,
            creditHour: `129`,
            Fee:`10,20,000`
        },
        {
            courseName:` MCS`,
            duration: `2 years`,
            creditHour: `12`,
            Fee:`6,20,000`
        },
    ]
  return (
      <div>FeeDetail
          {feeStructure.map((value, i) => {
              return (
                  <div> {value.courseName} {value.duration } { value.creditHour} { value.Fee}</div>
              )
            
          })
          }
      </div>
      
  )
}
