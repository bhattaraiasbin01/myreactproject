import React, { useEffect, useState } from 'react'

const UseEffectExample = () => {
    let [count1, setCount1] = useState(0)
    let[count2,setCount2]=useState(10)
    // useEffect(() => {
    //     console.log(`I am use effect and i excute at last`)
    // }, [count1, count2])
    // useEffect(() => {
    //     console.log(`I am use effect and i will excute if count1 changes`)]3
    // },[count1])
    useEffect(() => {
            console.log(`I am use effect and i will excute if count1 changes`)
        })//without dependancy will time it will be render
  return (
      <div>
          count1 is {count1}<br></br>
         count2 is {count2}
          
          <br></br>
          <button onClick={() => {
              setCount1(count1+1)
          }}>Count1</button>
           {/* <br></br> */}
          <button onClick={() => {
              setCount2(count2+1)
          }}>Count2</button>
    </div>
  )
}

export default UseEffectExample