import React, { useRef } from 'react'

const UseRef = () => {
    let widthChange=useRef()
  return (
      <div>
          <input placeholder='Name' ref={widthChange}></input><br></br>
          <button onClick={() => {
              widthChange.current.style.width="400px"
          }}>Change Width</button>

    </div>
  )
}

export default UseRef