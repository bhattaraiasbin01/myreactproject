import { computeHeadingLevel } from '@testing-library/react'
import React, { useState } from 'react'

const Form3 = () => {
    let [productName, setProductName] = useState("")
    let [productManagerEmail, setProductManagerEmail] = useState("")
    let [staffEmail, setStaffEmail] = useState("")
    let [password, setPassword] = useState("")
    let [gender, setGender] = useState("")
    let [productManufactoreDate, setProductManufactoreDate] = useState("")
    let [managerIsMarried, setManagerIsMarried] = useState(false)
    let [managerSpouse, setManagerSpouse] = useState("")
    let [productLocation, setProductLocation] = useState("")
    let [productDescription, setProductDescription] = useState("")
    let [productIsAvailable, setProductIsAvailable] = useState(true)
    let [country,setCountry]=useState("")
    let[cloth,setCloth]=useState("")
    // let[genderOption,setGenderOption]=useState("")

    let handleSubmit = (e) => {
        e.preventDefault()
        let info = {
            _productName: productName,
        _productManagerEmail:productManagerEmail,
        _staffEmail:staffEmail,
        _password:password,
        _gender:gender,
        _productManufactoreDate: productManufactoreDate,
        _managerIsMarried : managerIsMarried,
        _managerSpouse: managerSpouse,
        _productLocation: productLocation,
        _productDescription: productDescription,
        _productIsAvailable:productIsAvailable,
        _country:country,
        _cloth:cloth
        // _genderOption:genderOption

        }
        
        console.log(info)
    }
     let genderOption=[
        {
            label:"Male",
            value:"male"
        },
        {
            label:"Female",
            value:"female"
        },
        {
            label:"Other",
            value:"other"
        }
    
    ]
    let countryOption=[
        {
            label:"Select Country",
            value:"",
            disabled:true
        },
        {
            label:"Nepal",
            value:"nep"
        },
        {
            label:"India",
            value:"ind"
        },
        {
            label:"China",
            value:"chi"
        },
        {
            label:"Japan",
            value:"jap"
        },
        {
            label:"Korea",
            value:"kor"
        }
    ]
    let clothOption=[
        {
            label:"Select Size",
            value:"",
            disabled:true
        },
        {
            label:"Small",
            value:"S"
        },
        {
            label:"Medium",
            value:"M"
        },
        {
        label:"Large",
        value:"L" 
    },
    {
        label:"Extra Large",
        value:"XL"
    },
    {
        label:"Double Extra Large",
        value:"XXL"
    }

    ]
    
      
    

    

    
    
  return (
      <div>
          <form onSubmit={handleSubmit}>
          <label htmlFor='productName'>ProductName</label>
              <input
                id='productName'
                  type='text'
                  value={productName}
                  onChange={(e) => {
                    setProductName(e.target.value) 
              }}>
              </input>
              <br></br>
              <br></br>
              <br></br>

              <label htmlFor='productManagerEmail'>Product Manager Email</label>
              <input
                  id='productManagerEmail'
                  type='email'
                  value={productManagerEmail}
                  onChange={(e) => {
                  setProductManagerEmail(e.target.value)
                  }}></input>
              <br></br>
              <br></br>
              <br></br>
              <label htmlFor='staffEmail'>Staff Email</label>
              <input
                  id='Staff Email'
                  type='email'
                  value={staffEmail}
                  onChange={(e) => {
                  setStaffEmail(e.target.value)
              }}>
                  
              </input>
              
              <br></br>
              <br></br>
              <br></br>
              <label htmlFor='password'>Password</label>
              <input
                id='password'
                  type='password'
                  value={password}
                  onChange={(e) => {
                    setPassword(e.target.value) 
              }}>
              </input>
              <br></br>
              <br></br>
              <br></br>
              <label>Gender</label>
              <br></br>
              {genderOption.map((item,i)=>{
                return(
                <div key={i}>
                    <label htmlFor={item.value}>{item.label}</label>
                    <input
                    id={item.value}
                   value={item.value}
                   type="radio"
                   checked={gender===item.value}
                   onChange={(e)=>{
                    setGender(e.target.value)
                   }} >

                    </input>
                </div>
                )
              })}
              
              
              {/* <label htmlFor="male">Male</label>
              <input
                  value="male"
                  id="male"
                  type="radio"
                  checked={gender==="male"}
                  onChange={(e) => {
                      setGender(e.target.value)
                  }}>
              </input>
              <label htmlFor="female">Female</label>
              <input
                  value="female"
                  id="female"
                  type="radio"
                  checked={gender==="female"}
                  onChange={(e) => {
                      setGender(e.target.value)
                  }}>
              </input>
              <label htmlFor="other">Other</label>
              <input
                  value="other"
                  id="other"
                  type="radio"
                  checked={gender==="other"}
                  onChange={(e) => {
                      setGender(e.target.value)
                  }}>
                  </input> */}
              <br></br>
              <br></br>
              <br></br>
              <label htmlFor='productManufactoreDate'>Product Manufactore Date</label>
              <input
            id='productManufactoreDate'
                  type='date'
                  value={productManufactoreDate}
                  onChange={(e) => {
                      setProductManufactoreDate(e.target.value)
                  
              }}></input>

              <br></br>
              <br></br>
              <br></br>
              <label htmlFor='managerIsMarried'>Manager IsMarried</label>
              <input
                  id='managerIsMarried'
                  type='checkbox'
                  checked={managerIsMarried}
                  onChange={(e) => {
                  setManagerIsMarried(e.target.checked)
                  
              }}>
                  
              </input>

              <br></br>
              <br></br>
              <br></br>
             { managerIsMarried?
             <div><label htmlFor='managerSpouse'>Manager Spouse</label>
              <input
                  id='managerSpouse'
                  type='text'
                  value={managerSpouse}
                  onChange={(e) => {
                  setManagerSpouse(e.target.value)  }}></input></div>:null}

            {/* //   <label htmlFor='managerSpouse'>Manager Spouse</label>
            //   <input
            //       id='managerSpouse'
            //       type='text'
            //       value={managerSpouse}
            //       onChange={(e) => {
            //       setManagerSpouse(e.target.value)
             */}
              <br></br>
              <br></br>
              <br></br>
              <label htmlFor='productLocation'>Product Location</label>
              <input
                  id='productLocation'
                  type='text'
                  value={productLocation}
                  onChange={(e) => {
                  setProductLocation(e.target.value)
              }}></input>

              <br></br>
              <br></br>
              <br></br>
              <label htmlFor='productDescription'>Product Description</label>
              <textarea
                  id='productDescription'
                  value={productDescription}
                  onChange={(e) => {
                      setProductDescription(e.target.value)
                  }}
              ></textarea>
              <br></br>
              <br></br>
              <br></br>
              <label htmlFor='productIsAvailable'>Product IsAvailable</label>
              <input
                  id='productIsAvailable'
                  type='checkbox'
                  checked={productIsAvailable}
                  onChange={(e) => {
                      setProductIsAvailable(e.target.checked)
                      
                  }}
              >
                
              </input>
              <br></br>
              <br></br>
              <br></br>
              <label htmlFor='country'>Country</label>
              <select
              id="country"
              value={country}
              onChange={(e)=>{
                setCountry(e.target.value)
              }}>
              {
                countryOption.map((item,i)=>{
                    return(
                        
                            <option key={i} value={item.value} disabled={item.disabled}>
                                {item.label}
                            </option>
                        
                    )
                })
              }
             </select>
             <br></br>
              <br></br>
              <br></br>
              <label htmlFor="cloth">Cloth</label>
              <select
              id="cloth"
              value={cloth}
              onChange={(e)=>{
                setCloth(e.target.value)
              }}
              >
                {clothOption.map((item,i)=>{
                    return(
                        <option key={i} value={item.value} disabled={item.disabled}>{item.label}</option>
                    )
                })}
              </select>
              <br></br>
              <br></br>
              <br></br>
                  <button type='submit'>Submit</button>
              
          </form>
    </div>
  )
}

export default Form3