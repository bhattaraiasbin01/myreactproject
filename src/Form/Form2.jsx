import React, { useState } from 'react'

const Form2 = () => {
    let [name, setName] = useState("")
    let [age, setAge] = useState(0)
    let [email, setEmail] = useState("")
    let [gender, setGender] = useState("")
    let handleSubmit = (e) => {
        e.preventDefault()
        let info = {
            _name: name,
            _age: age,
            _email:email,
            _gender: gender,

        }
    }
    let genderOption = [
        {
            label: "Male",
            value:"male"
        },
        {
            label: "Female",
            value:"female"
        },
        {
            label: "Other",
            value:"other"
        },
    ]
  return (
      <div>
          <form onSubmit={handleSubmit}>
          <label htmlFor='name'>Name</label>
              <input
                id='name'
                  type='text'
                  value={name}
                  onChange={(e) => {
                    setName(e.target.value) 
              }}>
              </input>
              <br></br>
              <br></br>
              <label htmlFor='age'>Age</label>
              <input
                id='age'
                  type='number'
                  value={age}
                  onChange={(e) => {
                    setAge(e.target.value) 
              }}>
              </input>
              <br></br>
              <br></br>

              <label htmlFor='email'>Email</label>
              <input
                  id='email'
                  type='text'
                  value={email}
                  onChange={(e) => {
                      setEmail(e.target.value)
                  }}></input>
              {/* <br></br>
              <br></br>
              <label htmlFor="male">Male</label>
              <input
                  value="male"
                  id="male"
                  type="radio"
                  checked={gender==="male"}
                  onChange={(e) => {
                      setGender(e.target.value)
                  }}>
              </input>
              <label htmlFor="female">Female</label>
              <input
                  value="female"
                  id="female"
                  type="radio"
                  checked={gender==="female"}
                  onChange={(e) => {
                      setGender(e.target.value)
                  }}>
              </input>
              <label htmlFor="other">Other</label>
              <input
                  value="other"
                  id="other"
                  type="radio"
                  checked={gender==="other"}
                  onChange={(e) => {
                      setGender(e.target.value)
                  }}>
                  </input> */}
              
          </form>

    </div>
  )
}

export default Form2