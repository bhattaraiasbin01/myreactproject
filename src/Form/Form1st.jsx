import React, { useState } from 'react'

const Form1st = () => {
    let [name, setName] = useState("")
    let [age, setAge] = useState(0)
    let [email, setEmail] = useState("")
    let [password, setPassword] = useState("")
    
    let handleSubmit = (e) => {
        e.preventDefault()
        let info = {
            _name: name,
            _age: age,
            _email: email,
            _password:password
            
            }
    }
    
    
    
  return (
      <div>
          <form onSubmit={handleSubmit}>
              <label htmlFor='name'>Name</label>
              <input
                id='name'
                  type='text'
                  value={name}
                  onChange={(e) => {
                    setName(e.target.value) 
              }}>
              </input>
              <br></br>
              <br></br>
              <label htmlFor='age'>Age</label>
              <input
                id='age'
                  type='number'
                  value={age}
                  onChange={(e) => {
                    setAge(e.target.value) 
              }}>
              </input>
              <br></br>
              <br></br>

              <label htmlFor='email'>Email</label>
              <input
                id='email'
                  type='text'
                  value={email}
                  onChange={(e) => {
                    setEmail(e.target.value) 
              }}>
              </input>
              <br></br>
              <br></br>
              <label htmlFor='password'>Password</label>
              <input
                id='password'
                  type='password'
                  value={password}
                  onChange={(e) => {
                    setPassword(e.target.value) 
              }}>
              </input>
              <br></br>
              <br></br>
        <button type="submit">Submit</button>
          </form>
    </div>
  )
}

export default Form1st