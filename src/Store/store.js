//creating store

import { configureStore } from "@reduxjs/toolkit";
import counterSlice from "../feature/counterSlice";
import cricketSlice from "../feature/cricketSlice";
import foodSlice from "../feature/foodSlice";
import { productApi } from "../services/api/productServices";

// import { counterSlice } from "../feature/counterSlice";
// import addressSlice from "../feature/addressSlice";
// import blogSlice from "../feature/blogSlice";
// import counterSlice from "../feature/counterSlice";
// import productSlice from "../feature/productSlice";

export const store = configureStore({
  reducer: {
    info: counterSlice,
    cricket: cricketSlice,
    food: foodSlice,
    [productApi.reducerPath]: productApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat([productApi.middleware]),
});
