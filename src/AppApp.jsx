import React from 'react'
// import Form1 from '../../../ReactCloneNitanSir/learn-react-dw2-nitan/src/component/LForm/Form1'
import ChangeTitle from './Component/ChangeTitle'

import HideAndShow from './Component/HideAndShow'
import LearnUseState from './Component/LearnUseState'
import ObjInput from './Component/ObjInput'
import ObjState from './Component/ObjState'
import Form1st from './Form/Form1st'
import Form2 from './Form/Form2'
import Form3 from './Form/Form3'
import UseRef from './Form/UseRef'
import FormikComponentAll from './FormikHomeWork/FormikComponentAll'
import ProductInfo from './FormikHomeWork/ProductInfo'
import FormInput from './FormInputHitApi/FormInput'
import FormikForm from './LearnFormik/Form1Formik'
import FormikComponent from './LearnFormik/FormikComponent'
import FormikComponent2 from './LearnFormik/FormikComponent2'


import UseEffectExample from './LearnUseEffect/UseEffectExample'
import Parent from './ParentChild/Parent'
import RouteLearn from './Routing/ReactLearn'

const AppApp = () => {
  return (
      <div>
          {/* <LearnUseState></LearnUseState> */}
      {/* <ObjState></ObjState> */}
      {/* <ObjInput></ObjInput> */}
      {/* <Parent></Parent> */}
      {/* <UseEffectExample></UseEffectExample> */}
      {/* <ChangeTitle></ChangeTitle> */}
      {/* <HideAndShow></HideAndShow> */}
      {/* <Form1st></Form1st> */}
      {/* <Form2></Form2> */}
      {/* <Form3></Form3> */}
      {/* <FormikForm></FormikForm> */}
      {/* <FormikComponent></FormikComponent> */}
      {/* <FormikComponent2></FormikComponent2> */}
      {/* <FormikComponentAll></FormikComponentAll> */}
      {/* <ProductInfo></ProductInfo> */}
      {/* <UseRef></UseRef> */}
      {/* <RouteLearn></RouteLearn> */}
      <FormInput></FormInput>
    </div>
  )
}

export default AppApp