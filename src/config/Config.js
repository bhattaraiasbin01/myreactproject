export const baseUrl =
  process.env.REACT_APP_BASEURL || "https://project-dw.onrender.com/api/v1";

export const companyOptions = [
  {
    label: "Select Company",
    value: "",
    disabled: true,
  },
  {
    label: "Apple",
    value: "apple",
  },
  {
    label: "Samsung",
    value: "samsung",
  },
  {
    label: "Dell",
    value: "dell",
  },
  {
    label: "Mi",
    value: "mi",
  },
];

export const genderOption = [
  {
    label: "Male",
    value: "male",
  },
  {
    label: "Female",
    value: "female",
  },
  {
    label: "Other",
    value: "other",
  },
];
export const clothOption = [
  {
    label: "Select Size",
    value: "",
    disabled: true,
  },
  {
    label: "Small",
    value: "S",
  },
  {
    label: "Medium",
    value: "M",
  },
  {
    label: "Large",
    value: "L",
  },
  {
    label: "Extra Large",
    value: "XL",
  },
  {
    label: "Double Extra Large",
    value: "XXL",
  },
];
