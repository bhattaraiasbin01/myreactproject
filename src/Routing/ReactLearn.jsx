import React from "react";
import { NavLink, Route, Routes } from "react-router-dom";
import About from "./About";
import Contact from "./Contact";
import Error from "./Error";
import Home from "./Home";
import Other from "./Other";
import ProductDetails from "./ProductDetails";
import SearchParams from "./SearchParams";

const RouteLearn = () => {
  return (
    <div>
      <NavLink to="/" style={{ marginLeft: "20px" }}>
        Home
      </NavLink>
      <NavLink to="/about" style={{ marginLeft: "20px" }}>
        About
      </NavLink>
      <NavLink to="/contact" style={{ marginLeft: "20px" }}>
        Contact
      </NavLink>
      <NavLink to="/other" style={{ marginLeft: "20px" }}>
        Other
      </NavLink>
      <Routes>
        <Route path="/" element={<Home></Home>}></Route>
        <Route path="/about" element={<About></About>}></Route>
        <Route path="/contact" element={<Contact></Contact>}></Route>
        <Route path="/other" element={<Other></Other>}></Route>
        <Route path="*" element={<Error></Error>}></Route>
      </Routes>
      <NavLink to="product/:id1/id/:id2">Product Details</NavLink>
      <Routes>
        <Route
          path="product/:id1/id/:id2"
          element={<ProductDetails></ProductDetails>}
        ></Route>
      </Routes>
      <br/>
      <NavLink to="/searchparams">Search Params</NavLink>
      <Routes>
        <Route
          path="/searchparams"
          element={<SearchParams></SearchParams>}
        ></Route>
      </Routes>
    </div>
  );
};

export default RouteLearn;
