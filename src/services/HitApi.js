import axios from "axios";
import { baseUrl } from "../config/Config";

export let hitApi = axios.create({
  baseURL: baseUrl,
});
