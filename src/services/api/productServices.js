import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "../../config/Config";
// import { baseUrl } from "../../config/config";
// generally
//we provide tags to query (get)
//and we invalidate tags in mutation(other)
//and and
export const productApi = createApi({
  // reducerPath must be unique
  reducerPath: "productApi",
  baseQuery: fetchBaseQuery({
    baseUrl: baseUrl,
  }),
  // tag 1)creating tag
  tagTypes: ["readProduct", "readProductById"],

  //query and mutation
  //except get in other we use mutation as other task is  performed after button click

  endpoints: (builder) => ({
    readProduct: builder.query({
      query: () => {
        return {
          url: "/products",
          method: "GET", //must always be in capital
        };
      },
      //tag 2.provide that tag which was created above
      providesTags: ["readProduct"], // we create tag and provide tag in query and invalidate those tag in mutation
    }),
    readProductById: builder.query({
      query: (id) => {
        return {
          url: `/products/${id}`,
          method: "GET", //must always be in capital
        };
      },
      providesTags: ["readProductById"], // we create tag and provide tag in query and invalidate those tag in mutation
    }),
    deleteProduct: builder.mutation({
      query: (id) => {
        return {
          url: `/products/${id}`,
          method: "DELETE", //must always be in capital
        };
      },
      // tag3) invalidate Tag
      invalidatesTags: ["readProduct"],
    }),
    createProduct: builder.mutation({
      query: (body) => {
        return {
          url: "/products",
          method: "POST",
          body: body, //must always be in capital
        };
      },
      invalidatesTags: ["readProduct"],
    }),
    updateProduct: builder.mutation({
      query: (info) => {
        //here we cannot write 2 things so we call object here
        return {
          url: `/products/${info.id}`,
          method: "PATCH",
          body: info.body, //must always be in capital
        };
      },
      invalidatesTags: ["readProduct", "readProductById"],
    }),
    // invalidate detail page while update
  }),
});

// invalidatesTags: ["readProduct","readProductById"],

export const {
  useReadProductQuery,
  useDeleteProductMutation,
  useReadProductByIdQuery,
  useCreateProductMutation,
  useUpdateProductMutation,
} = productApi;
