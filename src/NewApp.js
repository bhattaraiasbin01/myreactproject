// import React, { useState } from "react";

// const NewApp = () => {
//   let [count, setCount] = useState(0);
//   let [name, setName] = useState("nitan");

//   //   let handClick = () => {
//   //     console.log("I am clicked");
//   let handClick = () => {
//     setCount(1);
//   };

//   let handlClick1 = () => {
//     setName("Shyam");
//   };

//   return (
//     //   <div onClick={handClick}>
//     <div>
//       {count}

//       <br></br>
//       <button onClick={handClick}>Click Me</button>
//       <br></br>
//       {name}
//       <button onClick={handlClick1}>Click Me</button>
//     </div>
//   );
// };
// export default NewApp;

// Q38  make  a component that is use for increment and decrement
// ie make a state variable initilize 0 as initialvalue
// make a button named Increment when it is clicked the variable must be increase by one
// make a button named Decrement whent it is clicked the variable must be decrease by one

import React, { useState } from "react";

const NewApp = () => {
  let [number, setNumber] = useState(0);

  let increment = () => {
    setNumber(number + 1);
  };
  let decrement = () => {
    setNumber(number - 1);
  };

  return (
    <div>
      {number}
      <br></br>
      <button onClick={increment}>Increment</button>
      <br></br>
      <button onClick={decrement}>Decrement</button>
    </div>
  );
};

export default NewApp;
// Q39 make a component that  perfom following thing
// make a state variable as name and initialized with initial value
// make a state variable as age and initialized with initial value
// make a state variable as address and initialized with initial value
// make a button as Change Age when it is click change age variable
// make a button as Change Name when it is click change name variable
// make a button as Change Address when it is click change address variable (edited)

// import React, { useState } from "react";

// const NewApp = () => {
//   let [name, setName] = useState(`Asbin`);
//   let [age, setAge] = useState(21);
//   let [address, setAddress] = useState(`Morang`);

//   let inputName = () => {
//     setName(`Ram`);
//   };
//   let inputAge = () => {
//     setAge(22);
//   };
//   let inputAddress = () => {
//     setAddress(`Jhapa`);
//   };

//   return (
//     <div>
//       {name}
//       <br></br>
//       <button onClick={inputName}>Change Name</button>
//       <br></br>
//       {age}
//       <br></br>
//       <button onClick={inputAge}>Change Age</button>
//       <br></br>
//       {address}
//       <br></br>
//       <button onClick={inputAddress}>Change Address</button>
//     </div>
//   );
// };

// export default NewApp;

// Q40   make  a component that is use for increment and reset
// ie make a state variable initilize 0 as initialvalue
// make a button named Increment when it is clicked the variable must be increase by one
// make a button named Reset when it is clicked it must be reset the varialbe to 0 ie make variable value as 0

// import React, { useState } from "react";

// const NewApp = () => {
//   let [number, setNumber] = useState(0);
//   let increment1 = () => {
//     setNumber(number + 1);
//   };
//   let reset = () => {
//     setNumber(0);
//   };
//   return (
//     <div>
//       {number}
//       <br></br>
//       <button onClick={increment1}>Increment</button>
//       <br></br>
//       <button onClick={reset}>Reset</button>
//     </div>
//   );
// };

// export default NewApp;
